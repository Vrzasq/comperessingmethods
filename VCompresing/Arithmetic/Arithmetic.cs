﻿using System.Collections.Generic;

namespace VCompresing.Arithmetic
{
    class Arithmetic
    {
        static int bitlen = 62;
        readonly ulong _MaxRange, _Half, _Quarter, _Quarter3;
        private DataModel _DataModel;
        private ulong currentDecodeValue, rangeHigh, rangeLow;
        private BitWriter _BitWriter;
        private BitReader _BitReader;
        long underflow;

        public Arithmetic()
        {
            _MaxRange = 1UL << bitlen;
            _Half = _MaxRange >> 1;
            _Quarter = _Half >> 1;
            _Quarter3 = 3 * _Quarter;
        }


        public byte[] Encode(IEnumerable<byte> data)
        {
            ResetEncoder(new DataModel(256));
            foreach (byte b in data)
                EncodeSymbol(b);
            EncodeSymbol(_DataModel.EOF);
            FlushEncoder();
            return _BitWriter.Data;
        }

        public byte[] Decode(IEnumerable<byte> data)
        {
            ResetDecoder(data, new DataModel(256));
            List<byte> output = new List<byte>();
            ulong symbol = 0;
            while ((symbol = DecodeSymbol()) != _DataModel.EOF)
                output.Add((byte)symbol);
            return output.ToArray();
        }

        public ulong OptimalLength
        {
            get
            {
                return _DataModel.OptimalLength;
            }
        }


        void EncodeSymbol(ulong symbol)
        {
            //Debug.Assert(rangeLow < rangeHigh);
            ulong range = rangeHigh - rangeLow + 1, left, right;
            _DataModel.GetRangeFromSymbol(symbol, out left, out right);

            ulong step = range / _DataModel.Total;
            //Debug.Assert(step > 0);
            rangeHigh = rangeLow + step * right - 1;
            rangeLow = rangeLow + step * left;

            _DataModel.AddSymbol(symbol);

            while ((rangeHigh < _Half) || (_Half <= rangeLow))
            {
                if (rangeHigh < _Half)
                {
                    _BitWriter.Write(0);
                    while (underflow > 0) { --underflow; _BitWriter.Write(1); }
                    rangeHigh = (rangeHigh << 1) + 1;
                    rangeLow <<= 1;
                }
                else
                {
                    _BitWriter.Write(1);
                    while (underflow > 0) { --underflow; _BitWriter.Write(0); }
                    rangeHigh = ((rangeHigh - _Half) << 1) + 1;
                    rangeLow = (rangeLow - _Half) << 1;
                }
            }
            while ((_Quarter <= rangeLow) && (rangeHigh < _Quarter3))
            {
                underflow++;
                rangeLow = (rangeLow - _Quarter) << 1;
                rangeHigh = ((rangeHigh - _Quarter) << 1) + 1;
            }

            //Debug.Assert(rangeHigh - rangeLow >= _Quarter);
        }

        void FlushEncoder()
        {

            if (rangeLow < _Quarter)
            {
                _BitWriter.Write(0);
                for (int i = 0; i < underflow + 1; ++i)
                    _BitWriter.Write(1);
            }
            else
            {
                _BitWriter.Write(1);
            }
            _BitWriter.Flush();
        }

        void ResetEncoder(DataModel _DataModel)
        {
            this._DataModel = _DataModel;

            rangeHigh = _Half + _Half - 1;
            rangeLow = 0;
            underflow = 0;
            _BitWriter = new BitWriter();
        }

        void ResetDecoder(IEnumerable<byte> data, DataModel _DataModel)
        {
            this._DataModel = _DataModel;

            rangeHigh = _Half + _Half - 1;
            rangeLow = 0;
            underflow = 0;

            currentDecodeValue = 0;
            _BitReader = new BitReader(data);

            for (int i = 0; i < bitlen; ++i)
                currentDecodeValue = (currentDecodeValue << 1) | _BitReader.Read();
        }

        ulong DecodeSymbol()
        {
            {
                ulong symbol = 0;
                //Debug.Assert(rangeLow < rangeHigh);
                ulong range = rangeHigh - rangeLow + 1, left, right;

                ulong step = range / _DataModel.Total;
                //Debug.Assert(step > 0);
                //Debug.Assert(currentDecodeValue >= rangeLow);
                //Debug.Assert(rangeHigh >= currentDecodeValue);
                ulong value = (currentDecodeValue - rangeLow) / step;
                symbol = _DataModel.GetSymbolAndRange(value, out left, out right);
                rangeHigh = rangeLow + step * right - 1;
                rangeLow = rangeLow + step * left;

                _DataModel.AddSymbol(symbol);

                while ((rangeHigh < _Half) || (_Half <= rangeLow))
                {
                    if (rangeHigh < _Half)
                    {
                        rangeHigh = (rangeHigh << 1) + 1;
                        rangeLow <<= 1;
                        currentDecodeValue = (currentDecodeValue << 1) | _BitReader.Read();
                    }
                    else
                    {
                        rangeHigh = ((rangeHigh - _Half) << 1) + 1;
                        rangeLow = (rangeLow - _Half) << 1;
                        currentDecodeValue = ((currentDecodeValue - _Half) << 1) | _BitReader.Read();
                    }
                }
                while ((_Quarter <= rangeLow) && (rangeHigh < _Quarter3))
                {
                    rangeLow = (rangeLow - _Quarter) << 1;
                    rangeHigh = ((rangeHigh - _Quarter) << 1) + 1;
                    currentDecodeValue = ((currentDecodeValue - _Quarter) << 1) | _BitReader.Read();
                }
                return symbol;
            }
        }
    }
}