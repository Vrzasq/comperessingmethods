﻿using System.Collections.Generic;

namespace VCompresing.Arithmetic
{
    class BitReader
    {
        public BitReader(IEnumerable<byte> data)
        {
            bitPos = 0;
            datum = 0;
            decodeByteIndex = 0;
            decodeData = data.GetEnumerator();
            decodeData.MoveNext();
            datum = decodeData.Current;
        }

        public byte Read()
        {
            byte bit = (byte)(datum >> 7);
            datum <<= 1;
            ++bitPos;
            if (bitPos == 8)
            {
                decodeByteIndex++;
                if (decodeData.MoveNext())
                    datum = decodeData.Current;
                bitPos = 0;
            }
            return bit;
        }

        byte datum;
        int bitPos;
        IEnumerator<byte> decodeData;
        long decodeByteIndex;
    }
}
