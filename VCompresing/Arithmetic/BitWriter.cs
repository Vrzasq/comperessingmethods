﻿using System.Collections.Generic;

namespace VCompresing.Arithmetic
{
    class BitWriter
    {
        public BitWriter()
        {
            bitPos = 0;
            encodeData = new List<byte>();
            datum = 0;
        }

        public void Write(byte bit)
        {
            datum <<= 1;
            datum = (byte)(datum | (bit & 1));
            ++bitPos;
            if (bitPos == 8)
            {
                encodeData.Add(datum);
                bitPos = 0;
            }
        }

        public void Flush()
        {
            while (bitPos != 0)
                Write(0);
        }

        public byte[] Data { get { return encodeData.ToArray(); } }

        int bitPos = 0;
        byte datum;
        List<byte> encodeData;
    }
}