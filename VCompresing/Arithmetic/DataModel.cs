﻿using System;

namespace VCompresing.Arithmetic
{
    class DataModel
    {
        public ulong EOF { get { return eof; } }

        public ulong Total { get; set; }

        public DataModel(ulong size)
        {
            eof = size;
            tree = new ulong[EOF + 2];

            for (int i = 0; i < tree.Length - 1; ++i)
                AddSymbol((ulong)i);
        }

        public void AddSymbol(ulong symbol)
        {
            long k = (long)symbol;
            for (; k < tree.Length; k |= k + 1)
                tree[k]++;
            ++Total;
        }

        public void GetRangeFromSymbol(ulong symbol, out ulong low, out ulong high)
        {

            ulong diff = tree[symbol];
            long b = (long)(symbol);
            long target = (b & (b + 1)) - 1;

            ulong sum = 0;
            b = (long)(symbol - 1);
            for (; b >= 0; b = (b & (b + 1)) - 1)
            {
                if (b == target) diff -= sum;
                sum += tree[b];
            }
            if (b == target) diff -= sum;
            low = sum;
            high = sum + diff;
        }

        public ulong GetSymbolAndRange(ulong value, out ulong low, out ulong high)
        {
            ulong N = (ulong)tree.Length;
            ulong bottom = 0;
            ulong top = N;
            while (bottom < top)
            {
                ulong mid = bottom + ((top - bottom) / 2);
                if (Query(0, (long)mid) <= value)
                    bottom = mid + 1;
                else
                    top = mid;
            }
            if (bottom < N)
            {
                GetRangeFromSymbol(bottom, out low, out high);
                //Debug.Assert((low <= value) && (value < high));
                return bottom;
            }
            //Debug.Assert(false, "Illegal lookup overflow!");
            low = high = UInt64.MaxValue;
            return UInt64.MaxValue;
        }

        public ulong OptimalLength
        {
            get
            {
                double sum = 0;
                double total = (Total - (ulong)tree.Length + 1);
                for (int i = 0; i < tree.Length - 1; ++i)
                {
                    double freq = Query(i, i) - 1;
                    if (freq > 0)
                    {
                        double p = freq / total;
                        sum += -Math.Log(p, 2) * freq;
                    }
                }
                return (ulong)(sum);
            }
        }

        ulong Query(long a, long b)
        {
            if (a == 0)
            {
                ulong sum = 0;
                for (; b >= 0; b = (b & (b + 1)) - 1)
                    sum += tree[b];
                return sum;
            }
            else
            {
                return Query(0, b) - Query(0, a - 1);
            }
        }

        ulong[] tree;
        ulong eof;
    }
}
