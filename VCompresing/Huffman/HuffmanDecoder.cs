﻿using System.Collections;
using System.IO;
using System.Windows.Forms;

namespace VCompresing.Huffman
{
    class HuffmanDecoder
    {
        private Node _Root;
        private string _OriginName;
        private int _OriginSize;
        private string _OriginExt;
        private byte[] _EncodedFile;

        public HuffmanDecoder(string path)
        {
            using (BinaryReader br = new BinaryReader(File.OpenRead(path)))
            {
                int treeSize = br.ReadInt32();
                _Root = Node.LoadTreeFromFile(br.ReadBytes(treeSize));
                _OriginSize = br.ReadInt32();
                _OriginName = br.ReadString();
                _OriginExt = _OriginName.Substring(_OriginName.Length - 4);
                int fileSize = br.ReadInt32();
                _EncodedFile = br.ReadBytes(fileSize);
            }
        }

        public byte[] Decode()
        {
            BitArray ba = new BitArray(_EncodedFile);
            byte[] decodedFile = new byte[_OriginSize];
            Node currentNode = _Root;

            int fileIndex = 0;
            for (int i = 0; i < ba.Length; i++)
            {
                if (ba[i]) // if 1 go right
                {
                    if (currentNode.RightNode != null) // if right node is not null go right
                        currentNode = currentNode.RightNode;
                }
                else
                {
                    if (currentNode.LeftNode != null) // if bit == 0 than go left
                        currentNode = currentNode.LeftNode;
                }

                if (currentNode.IsLeaf()) // 
                {
                    decodedFile[fileIndex] = currentNode.Char;
                    currentNode = _Root;
                    fileIndex++;
                    if (fileIndex == _OriginSize) // break when You reach number of bytes of orignal file
                        break;
                }
            }

            return decodedFile;
        }

        public void WirteToFile(byte[] EncodedFile)
        {
            SaveFileDialog sd = new SaveFileDialog();
            sd.DefaultExt = _OriginExt.Substring(1);
            sd.Filter = "Origin Name (" + _OriginExt + ") | *" + _OriginExt;

            if (sd.ShowDialog() == DialogResult.OK)
                File.WriteAllBytes(sd.FileName, EncodedFile);
                
            sd.Reset();
        }
    }
}
