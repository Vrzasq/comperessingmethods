﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace VCompresing
{
    class HuffmanEncoder : IDisposable
    {
        private byte[] _SourceMsg; // array of Bytes or chars to encode
        private LinkedList<Node> _NodeList = new LinkedList<Node>();
        private int[] _ByteCount;
        public Node Root { get; private set; } // Top of the tree / or the full tree 

        public HuffmanEncoder(byte[] arr)
        {
            _SourceMsg = arr;
            _ByteCount = CountBytes(arr);
            Build();
        }

        // Encode original msg
        public byte[] Encode()
        {
            BitArray[] encodingDictionary = CreateEncodingDictionary();

            //for (int i = 0; i < encodingDictionary.Length; i++)
            //{
            //    Debug.WriteLine(i + " " + encodingDictionary[i].ToDigitString());
            //}

            BitArray[] encodedMsg = new BitArray[_SourceMsg.Length];

            //Debug.Print("Encoding msg: Start");
            for (int i = 0; i < _SourceMsg.Length; i++)
            {
                // thi will save a lot of memory
                // but due to transformaiton increae CPU usage
                encodedMsg[i] = encodingDictionary[_SourceMsg[i]];
                //for (int j = bits.Length - 1; j >= 0; j--)
                //{
                //    Debug.Write(bits[j] ? "1" : "0");
                //}
                //Debug.Print(" " + temp[0]);
            }
            //Debug.Print("Encoding msg: Finished");

            BitArray bitArr = BitArrFromList(encodedMsg);
            byte[] msg = new byte[bitArr.Count / 8 + (bitArr.Length % 8 == 0 ? 0 : 1)];
            bitArr.CopyTo(msg, 0);

            return msg;
        }

        // 1 wirte binary tree lenght
        // 2 Write binary tree
        // 3 write original file lenght
        // 4 write original file name
        // 5 write encoded file Length
        // 6 write encoded file
        public void WriteToFile(byte[] encodedFile, string originFileName)
        {
            SaveFileDialog sd = new SaveFileDialog();
            string sufix = ".vhf";
            if (sd.ShowDialog() == DialogResult.OK)
            {
                string path = sd.FileName + sufix;

                byte[] treeBinary = Root.GetSerializedTree();

                FileStream fs = File.OpenWrite(path);
                BinaryWriter bw = new BinaryWriter(fs);

                //Debug.Print(treeBinary.Length.ToString());
                //Debug.Print(encodedFile.Length.ToString());

                bw.Write(treeBinary.Length);
                bw.Write(treeBinary);
                bw.Write(_SourceMsg.Length);
                bw.Write(originFileName);
                bw.Write(encodedFile.Length);
                bw.Write(encodedFile);

                // free resources
                bw.Close();
            }
        }

        // Create Array for increrasing performance
        // Traverse in worst scenerio will do 256 serch operations instead of byteArray.Length
        private BitArray[] CreateEncodingDictionary()
        {
            BitArray[] b = new BitArray[256];
            for (int i = 0; i < _ByteCount.Length; i++)
            {
                if (_ByteCount[i] > 0)
                {
                    List<bool> valueList = Root.Traverse(Convert.ToByte(i), new List<bool>());
                    b[i] = new BitArray(valueList.ToArray());
                }
            }
            return b;
        }

        private BitArray BitArrFromList(BitArray[] lb)
        {
            //Debug.Print("Start converting list to bitArray");
            int size = 0;
            foreach (BitArray ba in lb)
            {
                size += ba.Count;
            }
            BitArray b = new BitArray(size);
            int bIndex = 0;

            foreach (BitArray ba in lb)
            {
                for (int j = 0; j < ba.Length; j++)
                {
                    b[bIndex] = ba[j];
                    bIndex++;
                }
            }
            //Debug.Print("Finished converting list to bitarray");
            return b;
        }

        // we can assume that tab[index] in byte value so from 0 - 255
        private int[] CountBytes(byte[] array)
        {
            //Debug.Print("Counting symbols: Start");
            int[] b = new int[256];

            for (int i = 0; i < array.Length; i++)
            {
                b[array[i]]++;
            }
            //Debug.Print("Counting symbols: Finished");

            return b;
        }


        private void CreateLinkedList()
        {
            //Debug.Print("Creating LinkedList: Start");
            for (int i = 0; i < _ByteCount.Length; i++)
            {
                if (_ByteCount[i] > 0)
                    _NodeList.AddLast(new Node { Count = _ByteCount[i], Char = Convert.ToByte(i) });
            }
            //Debug.Print("Creating LinkedList: Finished");
        }

        // Build huffman tree and store it in Root Node
        private void Build()
        {
            CreateLinkedList();

            //Debug.Print("Building tree: Start");
            while (_NodeList.Count > 1)
            {
                // sort list Descending
                List<Node> l = new List<Node>(_NodeList.OrderByDescending(x => x.Count)); // cre

                if (_NodeList.Count >= 2)
                {
                    Node[] taken = { l[l.Count - 1], l[l.Count - 2] }; // take two last elemnts with lowest Count

                    Node parent = new Node() // create parent node by combining taken nodes
                    {
                        LeftNode = taken[0],
                        RightNode = taken[1],
                        Count = taken[0].Count + taken[1].Count
                    };

                    // remove taken nodes from list
                    _NodeList.Remove(taken[0]);
                    _NodeList.Remove(taken[1]);

                    // add parent node to list
                    _NodeList.AddLast(parent);
                }
            }

            Root = _NodeList.Last.Value;
            //Debug.Print("Building tree: Finished");
        }

        bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                _SourceMsg = null;
                _NodeList = null;
                _ByteCount = null;
                Root = null;
            }
            disposed = true;
        }
    }
}
