﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace VCompresing
{
    [Serializable]
    sealed class Node
    {
        public byte Char { get; set; }

        [NonSerialized]
        private int _Count;
        public int Count
        {
            get { return _Count; }
            set { _Count = value; }
        }

        public Node LeftNode { get; set; }
        public Node RightNode { get; set; }

        public bool IsLeaf()
        { return LeftNode == null && RightNode == null; }

        // Go through tree and return binary code of srearched character
        public List<bool> Traverse(byte ch, List<bool> data)
        {
            if (IsLeaf()) // if we are on the leaf, no other way to go
            {
                if (ch.Equals(Char)) // if searched character == character in current NODE
                {
                    //Debug.Print(traverseCount.ToString());
                    //traverseCount = 0;
                    return data;
                }
                else
                    return null; // if You are in the leaf but character not founf return null
            }
            else
            {
                List<bool> left = null;
                List<bool> right = null;

                if (LeftNode != null) // if left node is not null go Left
                {
                    List<bool> leftPath = new List<bool>();
                    leftPath.AddRange(data);
                    leftPath.Add(false);

                    left = LeftNode.Traverse(ch, leftPath); // Traverse throug RightNode - recursive
                }

                if (RightNode != null) // If right node is not null go right
                {
                    List<bool> rightPath = new List<bool>();
                    rightPath.AddRange(data);
                    rightPath.Add(true);
                    right = RightNode.Traverse(ch, rightPath); // Traverse throug RightNode - recursive
                }

                if (left != null) // if character was found on the left path
                {
                    return left;
                }
                else // if not return right path
                {
                    return right;
                }
            }
        }

        public byte[] GetSerializedTree()
        {
            IFormatter formatter = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream())
            {
                formatter.Serialize(stream, this);
                return stream.ToArray();
            }
        }

        public Node LoadTreeFromFile(Stream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream) as Node;
        }

        public static Node LoadTreeFromFile(byte[] buffer)
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new MemoryStream(buffer))
            {
                return formatter.Deserialize(stream) as Node;
            }
        }
    }
}
