﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace VCompresing.LZW
{
    class LzwDecode
    {   //TODO Work on decoder
        private string _FilePath;
        private Dictionary<int, List<byte>> _DecodeDictionary = new Dictionary<int, List<byte>>();

        public LzwDecode(string path)
        {
            _FilePath = path;
            _DecodeDictionary = CreateDictionary();
        }

        public void Decode()
        {
            List<int> encodedFile = ReadFromFile(_FilePath);
            List<byte> decoded = new List<byte>();

            //Debug.Print("Encoded file input on Decoding " + encodedFile.Count.ToString());

            List<byte> pattern = new List<byte>(); // take first item
            pattern.Add(_DecodeDictionary[encodedFile[0]][0]);
            //Debug.Print("First symbol " + pattern[0].ToString());
            encodedFile.RemoveAt(0);

            decoded.AddRange(pattern);
            int missing = 0;
            foreach (int c in encodedFile) // main loop
            {
                List<byte> symbol;
                if (c == _DecodeDictionary.Count)
                {
                    symbol = new List<byte>(pattern);
                    symbol.Add(pattern[0]);
                }
                else
                    symbol = new List<byte>(_DecodeDictionary[c]); // decode symbol

                //if (missing == 16)
                //{
                //    Debug.Print(c + " " + symbol.Count.ToString());

                //}
                decoded.AddRange(symbol); // add it to decoded file

                pattern.Add(symbol[0]);

                _DecodeDictionary.Add(_DecodeDictionary.Count, pattern);

                pattern = new List<byte>(symbol);
                missing++;

            }

            //decoded.Add()

            //Debug.Print("missing ? :" + missing);
            //Debug.Print("Decode d.Count" + " " + _DecodeDictionary.Count.ToString());
            WriteToFile(decoded);
        }

        private List<int> ReadFromFile(string path)
        {
            List<int> l = new List<int>();
            using (BinaryReader br = new BinaryReader(File.OpenRead(path)))
            {
                while (br.BaseStream.Position < br.BaseStream.Length)
                    l.Add(br.ReadInt32());
            }
            return l;
        }

        private Dictionary<int, List<byte>> CreateDictionary()
        {
            Dictionary<int, List<byte>> d = new Dictionary<int, List<byte>>();

            for (int i = 0; i < 256; i++)
            {
                List<byte> l = new List<byte>() { Convert.ToByte(i) };
                d.Add(i, l);
            }
            return d;
        }

        private void WriteToFile(List<byte> decoded)
        {
            int byteIndex = 1;

            using (SaveFileDialog sd = new SaveFileDialog())
            {
                if (sd.ShowDialog() == DialogResult.OK)
                {
                    //Logger log = new Logger("C:\\Users\\Vrzasq\\Desktop\\decode.txt");
                    using (BinaryWriter bw = new BinaryWriter(File.OpenWrite(sd.FileName)))
                    {

                        //log.StartLogging();
                        foreach (byte b in decoded)
                        {
                            //log.Log(byteIndex + " " + Convert.ToInt32(b).ToString());
                            bw.Write(b);
                            byteIndex++;
                        }
                    }
                    //log.StopLogging();
                    //Debug.Print("Decode byteIndex" + " " + byteIndex.ToString());
                    MainWindow.ShowDoneDialog();
                }
            }

        }

        private byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}