﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace VCompresing.LZW
{
    class LzwEncode
    {
        private Dictionary<string, int> _EncodingDictionary = new Dictionary<string, int>();
        private string _FilePath;

        public LzwEncode(string filePath)
        {
            _FilePath = filePath;
        }

        public void Encode()
        {
            byte[] file = File.ReadAllBytes(_FilePath);
            //Logger log = new Logger("C:\\Users\\Vrzasq\\Desktop\\encode.txt");
            //log.StartLogging();
            //for (int i = 1; i <= file.Length; i++)
            //{
            //    log.Log(i + " " + file[i - 1].ToString());
            //}
            //log.StopLogging();

            _EncodingDictionary = BasicDictionary();
            //Debug.Print("Original filelenght " + file.Length.ToString());

            List<int> encoded = new List<int>();
            int byteIndex = 1;
            string series = string.Empty;
            //log.StartLogging();
            foreach (byte b in file)
            {
                string byteString = ThreeDigit(Convert.ToInt32(b).ToString()); // read symbol from file
                string symbol = series + byteString; // add symbol to the series

                //log.Log(symbol + " " + series);

                int t;
                if (_EncodingDictionary.TryGetValue(symbol, out t)) // check if it is in dictionary
                {
                    series = symbol; // is yes set series define new series
                }
                else // if note add series to dictionary
                {
                    encoded.Add(_EncodingDictionary[series]);
                    _EncodingDictionary.Add(symbol, _EncodingDictionary.Count);
                    series = byteString; // begin new series starting with last used symbol
                }
                byteIndex++;
            }
            //log.StopLogging();
            if (!string.IsNullOrEmpty(series))
            {
                //Debug.Print("What was left " + series);
                encoded.Add(_EncodingDictionary[series]);
            }
            //Debug.Print("Encode d.Count " + _EncodingDictionary.Count.ToString());
            //Debug.Print("Encode filesize " + encoded.Count.ToString());
            //Debug.Print("Encode byteIndex " + byteIndex.ToString());
            WirteToFile(encoded);
        }

        // build basic dictionary from exising bytes
        private Dictionary<string, int> BasicDictionary()
        {
            Dictionary<string, int> basicDictionary = new Dictionary<string, int>();
            for (int i = 0; i < 256; i++)
            {
                basicDictionary.Add(ThreeDigit(i.ToString()), i);
            }

            return basicDictionary;
        }

        private string ThreeDigit(string s)
        {
            switch (s.Length)
            {
                case 1: return "00" + s;
                case 2: return "0" + s;
                default: return s;
            }
        }

        private void WirteToFile(IEnumerable<int> encoded)
        {
            using (SaveFileDialog sd = new SaveFileDialog())
            {
                if (sd.ShowDialog() == DialogResult.OK)
                {
                    using (BinaryWriter bw = new BinaryWriter(File.OpenWrite(sd.FileName + ".vfz")))
                    {
                        foreach (int item in encoded)
                            bw.Write(item);
                    }
                    MainWindow.ShowDoneDialog();
                }
                else
                    MessageBox.Show("File was not saved", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

        }

    }
}
