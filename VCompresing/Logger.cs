﻿using System.IO;

namespace VCompresing
{
    class Logger
    {
        private string _FilePath;
        private StreamWriter _Sw;

        public Logger(string filePath)
        { _FilePath = filePath; }

        public void StartLogging()
        {
            _Sw = new StreamWriter(_FilePath);
        }

        public void StopLogging()
        {
            _Sw.Close();
        }

        public void Log(string s)
        {
            _Sw.WriteLine(s);
        }
    }
}
