﻿namespace VCompresing
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_HufComp = new System.Windows.Forms.Button();
            this.btn_HufDecomp = new System.Windows.Forms.Button();
            this.btn_AriComp = new System.Windows.Forms.Button();
            this.btn_AriDecomp = new System.Windows.Forms.Button();
            this._btwLzwComp = new System.Windows.Forms.Button();
            this.btn_LzwDecomp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_HufComp
            // 
            this.btn_HufComp.Location = new System.Drawing.Point(90, 60);
            this.btn_HufComp.Name = "btn_HufComp";
            this.btn_HufComp.Size = new System.Drawing.Size(138, 35);
            this.btn_HufComp.TabIndex = 0;
            this.btn_HufComp.Text = "Huffman compress";
            this.btn_HufComp.UseVisualStyleBackColor = true;
            this.btn_HufComp.Click += new System.EventHandler(this.btn_HufComp_Click);
            // 
            // btn_HufDecomp
            // 
            this.btn_HufDecomp.Location = new System.Drawing.Point(90, 133);
            this.btn_HufDecomp.Name = "btn_HufDecomp";
            this.btn_HufDecomp.Size = new System.Drawing.Size(138, 35);
            this.btn_HufDecomp.TabIndex = 0;
            this.btn_HufDecomp.Text = "Hufmann decompress";
            this.btn_HufDecomp.UseVisualStyleBackColor = true;
            this.btn_HufDecomp.Click += new System.EventHandler(this.btn_HufDecomp_Click);
            // 
            // btn_AriComp
            // 
            this.btn_AriComp.Location = new System.Drawing.Point(263, 60);
            this.btn_AriComp.Name = "btn_AriComp";
            this.btn_AriComp.Size = new System.Drawing.Size(138, 35);
            this.btn_AriComp.TabIndex = 0;
            this.btn_AriComp.Text = "Arithmetic compress";
            this.btn_AriComp.UseVisualStyleBackColor = true;
            this.btn_AriComp.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_AriDecomp
            // 
            this.btn_AriDecomp.Location = new System.Drawing.Point(263, 133);
            this.btn_AriDecomp.Name = "btn_AriDecomp";
            this.btn_AriDecomp.Size = new System.Drawing.Size(138, 35);
            this.btn_AriDecomp.TabIndex = 0;
            this.btn_AriDecomp.Text = "Arithmetic decompress";
            this.btn_AriDecomp.UseVisualStyleBackColor = true;
            this.btn_AriDecomp.Click += new System.EventHandler(this.btn_AriDecomp_Click);
            // 
            // _btwLzwComp
            // 
            this._btwLzwComp.Location = new System.Drawing.Point(443, 60);
            this._btwLzwComp.Name = "_btwLzwComp";
            this._btwLzwComp.Size = new System.Drawing.Size(138, 35);
            this._btwLzwComp.TabIndex = 0;
            this._btwLzwComp.Text = "LZW compress";
            this._btwLzwComp.UseVisualStyleBackColor = true;
            this._btwLzwComp.Click += new System.EventHandler(this._btwLzwComp_Click);
            // 
            // btn_LzwDecomp
            // 
            this.btn_LzwDecomp.Location = new System.Drawing.Point(443, 133);
            this.btn_LzwDecomp.Name = "btn_LzwDecomp";
            this.btn_LzwDecomp.Size = new System.Drawing.Size(138, 35);
            this.btn_LzwDecomp.TabIndex = 0;
            this.btn_LzwDecomp.Text = "LZW decompress";
            this.btn_LzwDecomp.UseVisualStyleBackColor = true;
            this.btn_LzwDecomp.Click += new System.EventHandler(this.btn_LzwDecomp_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 258);
            this.Controls.Add(this.btn_LzwDecomp);
            this.Controls.Add(this.btn_AriDecomp);
            this.Controls.Add(this._btwLzwComp);
            this.Controls.Add(this.btn_AriComp);
            this.Controls.Add(this.btn_HufDecomp);
            this.Controls.Add(this.btn_HufComp);
            this.Name = "MainWindow";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_HufComp;
        private System.Windows.Forms.Button btn_HufDecomp;
        private System.Windows.Forms.Button btn_AriComp;
        private System.Windows.Forms.Button btn_AriDecomp;
        private System.Windows.Forms.Button _btwLzwComp;
        private System.Windows.Forms.Button btn_LzwDecomp;
    }
}

