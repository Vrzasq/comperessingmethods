﻿using System;
using System.IO;
using System.Windows.Forms;

namespace VCompresing
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        OpenFileDialog _fd = new OpenFileDialog();
        SaveFileDialog sd = new SaveFileDialog();

        private void btn_HufComp_Click(object sender, EventArgs e)
        {
            DialogResult dr = _fd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                using (HuffmanEncoder huf = new HuffmanEncoder(File.ReadAllBytes(_fd.FileName)))
                { huf.WriteToFile(huf.Encode(), _fd.SafeFileName); }

                MessageBox.Show("Done", "File Encoded", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            _fd.Reset();
        }

        private void btn_HufDecomp_Click(object sender, EventArgs e)
        {
            _fd.Filter = "Huffman compressed file (.vhf) | *.vhf";
            if (_fd.ShowDialog() == DialogResult.OK)
            {
                Huffman.HuffmanDecoder hufDe = new Huffman.HuffmanDecoder(_fd.FileName);
                hufDe.WirteToFile(hufDe.Decode());
                MessageBox.Show("Done", "File Decoded", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            _fd.Reset();
        }

        private void _btwLzwComp_Click(object sender, EventArgs e)
        {
            if (_fd.ShowDialog() == DialogResult.OK)
            {
                LZW.LzwEncode lzwEncode = new LZW.LzwEncode(_fd.FileName);
                lzwEncode.Encode();
            }
        }

        private void btn_LzwDecomp_Click(object sender, EventArgs e)
        {
            _fd.Filter = "LZW compressed file (.vfz) | *.vfz";
            if (_fd.ShowDialog() == DialogResult.OK)
            {
                LZW.LzwDecode lzwDecode = new LZW.LzwDecode(_fd.FileName);
                lzwDecode.Decode();
            }
            _fd.Reset();
        }

        public static void ShowDoneDialog()
        {
            MessageBox.Show("Done", "File Decoded", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (_fd.ShowDialog() == DialogResult.OK)
            {
                Arithmetic.Arithmetic ari = new Arithmetic.Arithmetic();
                byte[] encoded = ari.Encode(File.ReadAllBytes(_fd.FileName));
                using (SaveFileDialog sd = new SaveFileDialog())
                {
                    if (sd.ShowDialog() == DialogResult.OK)
                        File.WriteAllBytes(sd.FileName + ".var", encoded);
                    sd.Reset();
                }
            }
            _fd.Reset();
        }

        private void btn_AriDecomp_Click(object sender, EventArgs e)
        {
            _fd.Filter = "Arithmetic compressed file (.var) | *.var";
            Arithmetic.Arithmetic ari = new Arithmetic.Arithmetic();
            if (_fd.ShowDialog() == DialogResult.OK)
            {
                byte[] decoded = ari.Decode(File.ReadAllBytes(_fd.FileName));
                using (SaveFileDialog sd = new SaveFileDialog())
                {
                    if (sd.ShowDialog() == DialogResult.OK)
                        File.WriteAllBytes(sd.FileName, decoded);
                    sd.Reset();
                }
            }
            _fd.Reset();
        }
    }
}
