﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VCompresing
{
    static class MyExtensions
    {
        public static string ToDigitString(this BitArray array)
        {
            var builder = new StringBuilder();
            if (array != null)
            {
                foreach (var bit in array.Cast<bool>())
                    builder.Append(bit ? "1" : "0");
                return builder.ToString();
            }
            return "null";
        }
    }
}
